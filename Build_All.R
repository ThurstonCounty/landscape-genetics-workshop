rm( list=ls() )
library(tidyverse)
library( stringr )
library( knitr )
#
#  Open the "Terminal"
# 
#  git add -A
#  git commit -m 'put a message in here to explain what you are doing'
#  git push

# Fix to break in rmarkdown and HTMLWidgets
options(htmltools.preserve.raw = FALSE)
options(rmarkdown.html_vignette.check_title = FALSE)



#
# Scan all the RMD files and pull out libraries
#
if ( 0 ) {
  system("rm LandscapeGeneticPackages.R")
  
  
  allRmd <- list.files("lectures",pattern = ".Rmd",recursive = TRUE,full.names = TRUE)
  system("touch libs.R")
  for( file in allRmd) {
    purl( file, output = "packages.R")
    system( "cat packages.R | grep '^library' >> libs.R")
  }
  system("cat libs.R | uniq > packages.R")
  txt <- read.table("packages.R", header=FALSE, sep=")")$V1
  txt <- str_remove_all(txt, "library\\(")
  txt <- str_trim( txt ) %>% sort() %>% unique() 
  txt <- txt[ !(txt %in% c("gstudio","popgraph")) ]
  
  out <- paste( "if(!require(",txt,")){install.packages('",txt,"')}", sep=" ")
  

  txt <-  c("# ",
            "# Packages currently loaded in the workshop files.",
            "# ",
            "# The following packages were used in at least one file for this workshop and were automatically generated.",
            "# ",
            out, 
            "if( !require(gstudio) ) { remotes::install_github('dyerlab/popgraph') }",
            "if( !require(gstudio) ) { remotes::install_github('dyerlab/gstudio') }",
            "")
  
  
  fileCon <- file("LandscapeGeneticPackages.R")
  writeLines( txt , fileCon )
  close( fileCon )
  
  system("rm packages.R")
  system("rm libs.R")
  
  
}


# Build all the folders in the lecture tree
for( folder in list.dirs("lectures/basic",full.names = TRUE,recursive = FALSE) ) {
  print( paste("Building",folder) )
  blogdown::build_dir( folder, force=FALSE )
}

for( folder in list.dirs("lectures/spatial",full.names = TRUE,recursive = FALSE) ) {
  print( paste("Building",folder) )
  blogdown::build_dir( folder, force=FALSE )
}

for( folder in list.dirs("lectures/genetic",full.names = TRUE,recursive = FALSE) ) {
  print( paste("Building",folder) )
  blogdown::build_dir( folder, force=FALSE )
}

for( folder in list.dirs("lectures/landscape-genetic",full.names = TRUE,recursive = FALSE) ) {
  print( paste("Building",folder) )
  blogdown::build_dir( folder, force=FALSE )
}



rmarkdown::render("lectures/index.Rmd")




# Move over the contents of the lecture 
#system("cp -R lectures/ docs/")
system("rsync -raz --progress  lectures/ docs/")
system("find docs -iname '*.Rmd' -delete" )
system("find docs -iname '*.zip' -delete" )
system("find docs -type d -name template -exec rm -rf {} +")


# clean up the lecture folder
#system("find lectures -iname '*.html' -delete")
system("find lectures -iname 'Zoning_Districts.*' -delete")
system("find lectures -type d -name libs -exec rm -rf {} +")
system("find lectures -type d -name slides_files -exec rm -rf {} +")
system("find lectures -type d -name slides_cache -exec rm -rf {} +")
system("find lectures -type d -name activity_key_files -exec rm -rf {} +")
system("find lectures -type d -name Centerlines-shp -exec rm -rf {} +")
