---
title: "Introduction"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: ["default", "slide_styles.css"]
    seal: false
    nature:
      titleSlideClass: ["center","middle"]
      highlightStyle: default
      highlightLines: true
      ratio: "16:9"
      countIncrementalSlides: false
---
class: middle
background-image: url("background.png")
background-position: right
background-size: auto


```{r setup, include=FALSE}
library( sf )
library( knitr ) 
library( ggplot2 )
library( tidyverse )
library( kableExtra )
library( fontawesome )

knitr::opts_chunk$set( fig.retina = 3, 
                       warning = FALSE, 
                       message = FALSE,
                       fig.align="center")

theme_set( theme_classic( base_size = 20) )
```



# .orange[Spatial Data]

### .fancy[Projections & Coordinates `r fa("map-marker-alt", fill='red'  )`]


---
class: center, middle
background-image: url("https://live.staticflickr.com/65535/51201238932_dbd2eb50d9_c_d.jpg")
background-position: center
background-size: fill

---
class: bottom
background-image: url("https://live.staticflickr.com/65535/50437121667_ac9c3e7f84_c_d.jpg")
background-size: 55%


.footnote[ [Knight *et al.* (2005) *Nature*.  **437**, 880-883.](https://www.researchgate.net/deref/http%3A%2F%2Fdx.doi.org%2F10.1038%2Fnature03962)]



---

# Toblers First Law of Geography

.blue[
> Everything is related to everything else, but near things are more related to each other.
]

--

.pull-left[
### Physical Measurements
- Location  

- Distance  

- Network  

- Neighborhoods & Regions
]

.pull-right[
### Emerging Properties 

- Spatial Heterogeneity

- Spatial Dependence  

- Objects & Fields

]

.footnote[[Tobler, W. R. 1970. *Economic Geography*, **46**, 234–240.](https://doi.org/10.1002/9781118786352.wbieg1011)]








---
class: sectionTitle, inverse

# .orange[Projections]

## .fancy[Representing Location On .redinline[*This*] Planet.]



---

# A Projection

A projection is a mathematical mapping of points measured on this surface of this earth that can be represented on things like computer screens.

.center[![Example](https://live.staticflickr.com/65535/50437002623_e8df5b3c9f_c_d.jpg) ]








---
background-image: url("https://live.staticflickr.com/65535/50437046573_20efc64bdb_o_d.png")
background-position: right
background-size: auto
background-color: #00013B

# .blueinline[Earth == Lumpy <br>Bumpy Potato?]


&nbsp;

&nbsp;


## .redinline[Yes!    
&nbsp; &nbsp; &nbsp; - J. Ciminelli]



---

# Ellipsoids

Mathematical model of the physical structure of the surface of the planet.  

- NAD83/GRS80 - Satellite measurements of distance from core to surface of earth.  

- WGS84 - Model built on global GPS system.


---

# 🤷 How to Choose a Projection ️

In general, the following two rules should be considered when choosing a projection. 

*What is the purpose of your map?*

  - Maps that perserve angles, directions, distances, & shapes: *conformal projection*.  
  
  - Maps that preserve area, local densities, land use: *equal-area projections*.
  
  - Maps that preserve true-to-scale (locally), direction: *azimuthal equidistant*

&nbsp;

--


*Your projection should minimize distortion within the area of interest.*

  - Area of interest determines degree of concern.
  
  - Specialized local map projections may be available to center your area of interest.


---

# Example Data - Maps

For these examples, I'm going to be using the `maps` library<sup>1</sup>. .footnote[<sup>1</sup>If you do not have it, install it by `install.packages("maps")`.]For

```{r}
library( maps )
library( ggplot2 )
states <- map_data( "state" )
head(states, n=3)
```


---

# Basic Map From Polygon Points


.pull-left[
A basic map, notice the use of `group`.

```{r eval=FALSE}
ggplot( states, aes(x=long, 
                    y=lat, 
                    group=group) ) + 
  geom_polygon( fill="lightgray", 
                color="black", 
                lwd=0.25) + 
  theme_void() -> p

p
```
]

.pull-right[
```{r echo=FALSE}
ggplot( states, aes(x=long, y=lat, group=group) ) + 
  geom_polygon( fill="lightgray", color="black", lwd=0.25) + 
  theme_void() -> p
p
```
]




---

# On-The-Fly Projections

Some plotting facilities allow you to project your data *on the fly* (e.g., as you visualize it).

- Does not change the original data

- Only represents it in a different way


&nbsp;

--

&nbsp;

.redinline[This is only a convenience and should not be used for analyses.]


---

# Azimuth Projections


.pull-left[

Projected onto a 2-dimensional plane that is tangential to a single point on the surface of the earth (commonly  a pole, though not a necessity).

![Azequidistant](https://live.staticflickr.com/65535/50437120363_d8e0686d38_w_d.jpg)
]
.pull-right[
```{r}
p + coord_map( "azequalarea")
```

]

---

# Cylindrical Projections

.pull-left[
```{r}
p + coord_map( "cylindrical" )
```
]
.pull-right[
Parallels are straight lines and horizontal up and down the plot from latitude = 0

.center[
![](https://live.staticflickr.com/65535/50437120498_8dd67df3f1_w_d.jpg)
]
]


---

# Conic Projections


.pull-left[

Symmetric around the Prime Meridian and parallels are segments of concentric circles.

![](https://live.staticflickr.com/65535/50437120428_6da48bed81_w_d.jpg)

]
.pull-right[
```{r}
p + coord_map( "conic", lat0 = 30)
```

]



---

# Fooled by Projections 

.pull-left[
&nbsp;
![](https://live.staticflickr.com/65535/51186289722_2a7c3ecc05_d.jpg)
]

--

.pull-right[
![](https://live.staticflickr.com/65535/51187204013_5bf487c636_z_d.jpg)
]


---

# General Recommendations - Distance 

The following are some general guidelines for projections where you are attempting to minimize any bias for measurements of distance.

```{r echo=FALSE}
tibble( Property = "Equidistant",
            `At the Pole` = "Azimuthal Equidistant",
            `At the Equator` = "Plate Carree",
            Intermediate = "Equidistant Conic") %>%
  kable() %>%
  kable_paper() %>%
  add_header_above( c(" ", "Center of Projection"=3),
                    background=rep("white",4))
```

---
# Projections For Area & Conformations

The following projection guidelines are relevant if your research question focuses on measurements of conformation (angles, shapes, etc.) as well as estimates of area.


```{r echo=FALSE}
tibble( Property = c("Conformal", "Equal-Area"),
        `North/South` = c("Tranverse Mercator", "Sinusoidal"),
        `East/West Equator` = c("Mercator", "Cylindirical Equal Area"),
        `East/West` = c("Lambert Conforrmal Conic"),
        `Polar` = c("Sterographic", "Lambert Azimuth"),
        `Equatorial` = c("Sterographic", "Lambert Azimuth"),
        `Oblique` = c("Sterographic", "Lambert Azimuth")) %>%
  kable() %>%
  kable_paper() %>%
  add_header_above(c("", "N/S","E/W"=2, "All"=3)) %>%
  add_header_above(c("","Area of Interst Extends Mostly.."=6))
```






---
class: sectionTitle, inverse

# .green[Datum]


---

# Coordinate Systems

The *Datum* are the coordinate system used on the ellipsoid.  Common types include:

- Longitude & Latitude - The East/West & North/South position on the surface of the earth.

  - Prime Meridian (0° Longitude) passes thorugh the [Royal Observatory](https://en.wikipedia.org/wiki/Royal_Observatory,_Greenwich) in Greenwich England, with positive values of longitude to the east and negative to the west.
  
  - Equator (0° Latitude) and is defined as the point on the planet where both northern and southern hemisphers have equal amounts of day and night at the [equinox](https://en.wikipedia.org/wiki/Equinox) (Sept. 21 & March 21).
  
  - Richmond, Virginia: 37.533333 Latitude, -77.466667 Longitude
  
--


- Universal Trans Mercator - A division of the earth into 60 zones (~6°longitude each, labeled 01 - 60) and 20 bands each of which is ~8° latitude (labeled C-X excluding I & O with A & B dividing up Antartica).  See image [here](https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system#/media/File:Universal_Transverse_Mercator_zones.svg).

  - Coordinates include Zone & band designation as well as coordinates in Easting and Northing (planar coordinates within the zone) measured in meters.
  
  - Richmond, Virginia: 18S 282051 4156899






---

# Functional Consequences


&nbsp;

--

&nbsp;

.center[ 
.red[DATA NEED TO BE IN THE SAME ELLIPSOID & DATUM 

.red[FROM the BEGINNING]]
]





---
class: sectionTitle, inverse

# .orange[Known Projections]  `r fa("video", fill='red') `

## .fancy[Where do we find these things?]


---
background-image: url("https://live.staticflickr.com/65535/50445538736_bc1c3456e5_k_d.jpg)
background-size: cover



???

3857 Google 
4236 GPS
4484 UTM Mexico West

























---
class: sectionTitle, inverse 

## .green[Basic Visualization]  `r fa("binoculars", fill='red'  )`


---

# Beetle Data

The Bark Beetle, *Araptus attenuatus*, is a Sonoran Desert endemic parasite that lives on within the plant *Euphorbia lomelii*.

.pull-left[

&nbsp;

![Araptus attenuatus](https://live.staticflickr.com/65535/50441339417_74e04216fa_w_d.jpg)
]

.pull-right[
.center[![Euphorbia lomelii](https://live.staticflickr.com/65535/50441175211_ba3b9df2ea_w_d.jpg)]
]

---

# Sampling Sites


```{r}
library( readr )
url <- "https://raw.githubusercontent.com/dyerlab/ENVS-Lectures/master/data/Araptus_Disperal_Bias.csv"
read_csv( url ) %>%
  select( Site, Longitude, Latitude, everything() ) %>%
  arrange( Latitude ) -> data 
summary( data )
```





---

# GGPlot Maps

.pull-left[
```{r eval=FALSE}
data %>%
  ggplot( aes(Longitude, Latitude, size=MFRatio) ) +
  geom_point() +
  coord_map()
```
]

.pull-right[
```{r echo=FALSE}
data %>%
  ggplot( aes(Longitude, Latitude, 
              size=MFRatio) ) +
  geom_point() +
  coord_map()
```
]


---

# Map Overlays - Polygon 

```{r}
map_data("world","mexico") %>%
  rename(Longitude = long,
         Latitude = lat) -> world
  
head( world )
```


---

# Map Overlays - Polygon

.pull-left[
```{r eval=FALSE}
data %>%
  ggplot( aes(Longitude, Latitude )) + 
  geom_polygon( aes(group=group), 
                data=world, 
                fill="#cccccc") +
  geom_point( aes(size=MFRatio),
              alpha=0.75) +
  coord_sf( xlim = c(-115, -108),
            ylim = c(22, 30),
            crs = 4326 ) + 
  theme( axis.text.x = element_text( angle=45,
                                     hjust=1)) -> p
p
```

&nbsp;

Note: When plotting using polygons **and** restriction the x- or y- limits, you must use `coord_sf` rather than `xlim`/`ylim` else your maps will look .redinline[seriously wrong].  
]

.pull-right[
```{r echo=FALSE}
data %>%
  ggplot( aes(Longitude, Latitude )) + 
  geom_polygon( aes(group=group), 
                data=world, 
                fill="#cccccc") +
  geom_point( aes(size=MFRatio),
              alpha=0.75 ) +
  coord_sf( xlim = c(-115, -108),
            ylim = c(22, 30),
            crs = 4326) + 
  theme( axis.text.x = element_text( angle=45,
                                     hjust=1)) -> p
p
```

]

---

# Adding Map Components

.pull-left[

You must also specify a CRS to the `coord_sf()` call if you are going to overlay a scale bar so it knows what the right conversion of coordinate datum to distance is.

```{r eval=FALSE}
library( ggspatial )
p + 
  annotation_scale( location = "bl",
                    style = "bar") + 
  annotation_north_arrow(location = "bl", 
                width = unit(.5, "cm"),
                height = unit(.75, "cm"),
                pad_y = unit(0.75, "cm"),
                pad_x = unit(0.5, "cm"),
                style = north_arrow_orienteering(
                           text_size = 8
                         )) 
```

]


.pull-right[
```{r echo=FALSE}
library( ggspatial )
p + 
  annotation_scale( location = "bl",
                    style = "bar") + 
  annotation_north_arrow(location = "bl", 
                width = unit(.5, "cm"),
                height = unit(.75, "cm"),
                pad_y = unit(0.75, "cm"),
                pad_x = unit(0.5, "cm"),
                style = north_arrow_orienteering(
                           text_size = 8
                         )) 
```

]

---

# Raw Text Annotations


.pull-left[
```{r eval=FALSE}
p + 
  annotate( geom="text",
            x = -113,
            y = 23, 
            label = "Pacific Ocean",
            fontface = "italic",
            color= "dodgerblue3", 
            size=6) 
```

]

.pull-right[
```{r echo=FALSE}
p + 
  annotate( geom="text",
            x = -113,
            y = 23, 
            label = "Pacific Ocean",
            fontface = "italic",
            color= "dodgerblue3", 
            size=6) 
```
]





---

# Repel Labels

Useful a bit later when we start getting dense overlays of data.  The `ggrepel` library moves the labels around so that they minimize overlap.


```{r}
library( ggrepel )
pts <- data.frame( Country = c("United States", "Mexico", "Guatemala", 
                               "Belize", "El Salvador", "Hondouras", 
                               "Nicaragua","Costa Rica", "Cuba"),
                   Longitude = c(-97, -101, -91, -89, -89, -87, -85.5, -84, -84),
                   Latitude = c(34, 24, 16, 17, 14, 16, 13, 10, 23) )
```



---

# Automagical Label Moving

.pull-left[
```{r eval=FALSE}
map_data("world") %>%
  rename(Longitude = long,
         Latitude = lat) %>%
  ggplot( aes(Longitude, Latitude) ) +
  geom_polygon( aes(group=group), 
                fill="#cccccc", 
                color="#ffffff") + 
  geom_text_repel( aes(label=Country), 
                    data = pts, 
                   size=5) + 
  coord_sf( xlim = c(-115, -85),
            ylim = c(10, 35) ) 
```
]

.pull-right[
```{r echo=FALSE}
map_data("world") %>%
  rename(Longitude = long,
         Latitude = lat) %>%
  ggplot( aes(Longitude, Latitude) ) +
  geom_polygon( aes(group=group), 
                fill="#cccccc", 
                color="#ffffff") + 
  geom_text_repel( aes(label=Country), 
                    data = pts,
                   size=5) + 
  coord_sf( xlim = c(-115, -85),
            ylim = c(10, 35) ) 
```

]



---

# Inset Maps

It is possible to make two maps and then overlay them on top to provide context.  Here is an inset map with indication of a study area and customizations of the underlying theme to remove axis and grid components.

```{r}
map_data("world","mexico") %>%
  ggplot(aes(long,lat)) +
  geom_polygon( aes(group=group),
                fill="dodgerblue1") + 
  geom_point( data = data.frame( long=-111,
                                 lat = 24.2),
              size=3, 
              color="red") +
  theme_gray() + 
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank()) +
  coord_map() -> mexico
```

---

# Inset Map

```{r echo=FALSE}
mexico
```



---

# Inset Maps

.pull-left[
And here is the main map.

```{r eval=FALSE}
data %>%
  filter( Latitude < 25,
          Latitude > 24  ) -> local_sites

map_data("world","mexico") %>%
  rename( Longitude = long, 
          Latitude = lat ) %>%
  ggplot(aes(Longitude, Latitude) ) +
  geom_polygon( aes( group=group),
                fill="#eeeeee") + 
  geom_text_repel( data= local_sites, 
                   aes(label=Site)) + 
  coord_sf( xlim=c(-113,-109),
            ylim = c(23, 26)) -> site_map
site_map  
```
]

.pull-right[
```{r echo=FALSE}
data %>%
  filter( Latitude < 25,
          Latitude > 24  ) -> local_sites

map_data("world","mexico") %>%
  rename( Longitude = long, 
          Latitude = lat ) %>%
  ggplot(aes(Longitude, Latitude) ) +
  geom_polygon( aes( group=group),
                fill="#cccccc") + 
  geom_text_repel( data= local_sites, 
                   aes(label=Site),
                   size=4) + 
  coord_sf( xlim=c(-113,-109),
            ylim = c(23, 26)) -> site_map
site_map  
```

]




---

# Combining Plots

.pull-left[

Using the `cowplot` drawing functions, we can overlay these maps onto a single output.

```{r eval=FALSE}
library( cowplot )
ggdraw( site_map ) + 
  draw_plot( mexico, 
             width = 0.2,
             height = 0.2, 
             x = .75,
             y = .75)
```

]


.pull-right[
```{r echo=FALSE}
library( cowplot )
ggdraw( site_map ) + 
  draw_plot( mexico, 
             width = 0.2,
             height = 0.2, 
             x = .75,
             y = .6)
```

]


---

# A Leaflet Maps


.pull-left[
`Leaflet` is an interactive mapping library that allows you to quickly visualize your data.

```{r}
library( leaflet )
```

By default, `leaflet` assumes that your data are in latitude & longitude coordinate space.

]

--

.pull-right[
The implementation of the library requires:  

- Making a canvas `leaflet()`  

- Adding some markers with coordinates `addMarkers()` that may include labels as well.

- Adding Tiles `addTiles()` or `addProviderTiles()`
]


---

# A Simple Map - Leaflet


.pull-left[

```{r eval=FALSE}
library( leaflet )
data %>%
  mutate( Label = paste( "Site:", Site, 
                         "<hr>\nFemales:", Females, 
                         "<br>Males: ", Males,
                         "<br>Suitability:", Suitability) ) %>%
  leaflet() %>%
  addMarkers( ~Longitude, 
              ~Latitude, 
              popup = ~Label ) %>%
  addProviderTiles( "OpenTopoMap" )
```
]

.pull-right[
```{r echo=FALSE}
library( tidyverse )
library( leaflet )
data %>%
  mutate( Label = paste( "Site:", Site, 
                         "<hr>\nFemales:", Females, 
                         "<br>Males: ", Males,
                         "<br>Suitability:", Suitability) ) %>%
  leaflet() %>%
  addMarkers( ~Longitude, ~Latitude, popup = ~Label ) %>%
  addProviderTiles( "OpenTopoMap" )
```
]











---

class: middle
background-image: url("https://live.staticflickr.com/65535/50367566131_85c1285e2f_o_d.png")
background-position: right
background-size: auto


.pull-left[ ![](https://media.giphy.com/media/03g9zDwQ95MyB08oc0/giphy.gif) ]
.pull-right[ # 🙋🏻  Questions?

If you have any questions for about<br/> the content presented herein<br/> now is your time.  

If you think of something later though, <br/>feel free to [ask me via email](mailto://rjdyer@vcu.edu) and I'll<br/> get back to you as soon as possible.
]

