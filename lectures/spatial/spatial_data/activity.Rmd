---
title: "Spatial Data Activity"
output: html_notebook
---

![](https://live.staticflickr.com/65535/51022901036_7285939ab3_k_d.jpg)


## Overview

Tidyverse allows you to easily manipulate and analyze data in a way that both readable and maintainable.

## Activity Objective

For this activity, you'll need the following libraries.

```{r}
library( readr )
library( mapproj )
library( ggrepel )
library( cowplot )
library( tidyverse )
```

and the url for the *Araptus* data set is located here.

```{r}
url <- "https://raw.githubusercontent.com/dyerlab/ENVS-Lectures/master/data/Araptus_Disperal_Bias.csv"
```

For review, here are links to the [slides](https://thurstoncounty.gitlab.io/landscape-genetics-workshop/spatial/spatial_data/slides.html) on this topic.


---

# Activity Questions

Use the packages and data linked above to answer the following questions.

1. Use `map_data()` to get the polygon representing your home country and compare coordinate projections using `coord_map()` for at least 4 different projections listed in `?mapproj`

1. Create some coordinates representing at least five locations of interest to you (homes lived in, schools attended, favorite breweries, etc.).  Using `map_data()` create a map showing these locations with appropriate labels.

1. Make an inlay for the country/countries where the coordinates exist.  

1. Create a `leaflet` map of these locations with annotations for each such as a name and short description.  [Here](https://rstudio.github.io/leaflet/) is a nice resource for documentation on `leaflet` plotting in R.

1. Explore `?leaflet::addProviderTiles` to see additional backgrounds that can be added to your interactive maps.
