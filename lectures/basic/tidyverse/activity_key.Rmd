---
title: "Tidyverse Activity"
output: html_notebook
---

![](https://live.staticflickr.com/65535/51022165043_59a7f7b3d5_c_d.jpg)

## Overview

Tidyverse allows you to easily manipulate and analyze data in a way that both readable and maintainable.

## Activity Objective

For this activity, you'll need the following libraries.

```{r}
library( readr )
library( knitr )
library( lubridate )
library( tidyverse )
library( kableExtra )
```

and the url for the raw data set.

```{r}
url <- "https://docs.google.com/spreadsheets/d/1Mk1YGH9LqjF7drJE-td1G_JkdADOU0eMlrP01WFBT8s/pub?gid=0&single=true&output=csv"
```

For review, here are links to the [slides](slides.html) and [narrative](narrative.nb.html) on this topic.

---

# Activity Questions

1. Using tidyverse load in the data and format it properly including:
  - Make columns for `Date` representing a `POSIX` date object.
  - Create a `Weekday` column as an appropriately ordered factor.
  - Convert all data columns that are in Imperial Units (F, mph, in, etc.) to proper SI coordinates.
  - Drop any duplicate or irrelevant columns of data to minimize memory footprint.
  - Remove any rows that have missing data
  - Retain data columns for Date, Weekday, Wind Speed, Wind Direction, Rain, Air Temperature, Water Temperature, PH, Salinity, and Water Depth.
  
```{r}
format <- "%m/%d/%Y %I:%M:%S %p"
days <- c("Monday","Tuesday","Wednesday","Thursday", "Friday","Saturday", "Sunday")

read_csv( url ) %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders=format,
                                  tz="EST") ) %>%
  mutate( Weekday = factor( weekdays( Date ),
                            ordered=TRUE,
                            levels=days) ) %>%
  mutate( AirTemp = (AirTempF - 32) * 5/9,
          Rain = Rain_in * 2.54,
          `Wind Speed` = 1.60934 * WindSpeed_mph ) %>%
  select( Date, 
          Weekday, 
          Rain,
          `Wind Speed`, 
          `Wind Direction` = WindDir,
          `Air Temperature (°C)` = AirTemp,
          `Water Temperature (°C)` = H2O_TempC,
          PH, 
          Salinity = Salinity_ppt, 
          `Water Depth (m)` = Depth_m ) %>%
  filter( !is.na( PH) ) -> rice 

summary( rice )

```
  
  
  
2. On April 23, 1971, the Carpenters released the song "Rainy Days and Mondays Always Get Me Down".  Here is a [youtube](https://youtu.be/PjFoQxjgbrs) for those who may be too young to know this one.  From the data set, make a plot of total rain for the month of February by each day.  Are there significantly different amounts of rain by weekday?  

```{r fig.cap="The distribution of rain amounts by weekday for the month of February measured at the Rice Rivers Center.  Note, the amount of rain is log-transformed for to create a more readable visualization.", message=FALSE}
rice %>%
  filter( Date > ymd("20140201")  ) %>%
  filter( Date < ymd("20140301") ) %>% 
  filter( Rain > 0 ) %>%
  ggplot( aes(Weekday, Rain ) ) + 
  geom_boxplot() + 
  theme( axis.text.x = element_text(angle = 45, hjust=1 )) + 
  scale_y_log10()
```

3. Is there a prevailing wind direction for this field station?

```{r}
rice %>%
  ggplot( aes(`Wind Direction`) ) + 
  geom_histogram( bins=24) +
  coord_polar( start=0 ) + 
  theme_bw()
```

4. This field station is located in the tidal freshwater region of the Chesapeake Bay in Virginia, USA.  As such, there is a tidal surge.  Make a table of minimum and maximum tide depth for each day of the week that contained Valentines Day (14 February 2014).

```{r}
rice %>%
  filter( Date < ymd("2014-02-17"),
          Date >= ymd("2014-02-10")) %>%
  group_by( Weekday ) %>% 
  summarize( High = max(`Water Depth (m)`),
             Low = min( `Water Depth (m)`) ) %>%
  kable( digits=2,
         caption = "Tides measured at the Rice Rivers Center (as depth in meters) for each day of the week containing valentines day in 2014.") %>%
  kable_styling( full_width = FALSE ) %>%
  add_header_above(c("","Measured Tide Depth (m)"=2))
```




