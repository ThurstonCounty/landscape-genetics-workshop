---
title: "Introduction"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: ["default", "slide_styles.css"]
    seal: false
    nature:
      titleSlideClass: ["center","middle"]
      highlightStyle: default
      highlightLines: true
      ratio: "16:9"
      countIncrementalSlides: false
---
class: middle
background-image: url("background.png")
background-position: right
background-size: auto


```{r setup, include=FALSE}
knitr::opts_chunk$set( fig.retina = 3, 
                       warning = FALSE, 
                       message = FALSE,
                       fig.align="center")
library( knitr )
library( tidyverse )
library( DiagrammeR )
library( fontawesome )
library( DiagrammeRsvg )

options(knitr.table.format = "html")
options(htmltools.preserve.raw = FALSE)
theme_set( theme_classic( base_size = 20) )
```


# .orange[Workflow Judo🥋]


## .fancy[Welcome to the Tidyverse]


---
background-image: url("https://live.staticflickr.com/65535/50362129663_0d640ad239_k_d.jpg")
background-size: cover

# Data Operators

.pull-left[
There are a finite number of actions (verbs) that we can use on the raw data we work with.  

They can be combined to yield meaninful (or quimsical) inferences from our data:

&nbsp; 

.redinline[ &nbsp; Is there more sun on Fridays than on the weekend?]

&nbsp; 

.orangeinline[ &nbsp; What is the distribution of high-tide depths for each <br>&nbsp; day in January?]

&nbsp; 

.blueinline[ &nbsp; Is there a visible relationship between water salinity &<br>&nbsp;  measured pH?]


]



---
background-image: url("https://live.staticflickr.com/65535/50362827791_a32934b310_k_d.jpg")
background-size: cover

# Select

.pull-left[
Identify only subset of data columns that you are interested in using.]


---
background-image: url("https://live.staticflickr.com/65535/50362989322_6aa00c8398_k_d.jpg")
background-size: cover

# Filter

.pull-left[
Use only some subset of rows in the data based upon qualities wihtin the columns themselves.
]


---
background-image: url("https://live.staticflickr.com/65535/50362827946_d8d5508dfd_k_d.jpg")
background-size: cover

# Mutate

.pull-left[
Convert one data type to another, scaling, combining, or making any other derivative component.
]


---
background-image: url("https://live.staticflickr.com/65535/50362129893_61851436c8_k_d.jpg")
background-size: cover

# Arrange


.pull-left[
Reorder the data using values in one or more collumns to sort.
]


---
background-image: url("https://live.staticflickr.com/65535/50362869456_c869b2a0a9_k_d.jpg")
background-size: cover

# Group


.pull-left[
Partition the data set into groups based upon some taxonomy of categorization.
]


---
background-image: url("https://live.staticflickr.com/65535/50362989492_d4e281b741_k_d.jpg")
background-size: cover

# Summarize

.pull-left[

Perform operations on the data to characterize trends in the raw data as summary statistics.

]


---

# Combinations Yield Inference

Combining these actions together is how we perform the analyses.



```{r echo=FALSE, out.width='75%', out.height='30%', fig.align='center'}
graph <-
  create_graph() %>%
  add_path(
    n = 5,
    type = "step",
    label = c(
      "Load\\nData",
      "Select\\nColumns",
      "Overlay\\nPoints",
      "Overlay\\nTrend",
      "Show Plot"
    ),
    node_aes = node_aes(
      shape = c("square", "circle", "circle", "circle", "rectangle"),
      width = c(0.75, 0.75, 0.75, 0.75, 0.75),
      color = "#3C3C3C",
      fontcolor = "black",
      fillcolor = c("#61acf0", "#f0a561", "#f0a561", "#f0a561", "#cbd20a"),
      fontname = "Lato"
    ),
    edge_aes = edge_aes(# set edge aesthetics
      color = "#3C3C3C")
  ) %>%
  add_global_graph_attrs(attr = "layout",
                         value = "dot",
                         attr_type = "graph") %>%
  add_global_graph_attrs(attr = "rankdir",
                         value = "LR",
                         attr_type = "graph")

# View the graph in the Viewer
graph %>% render_graph()
```

--

```{r echo=FALSE, out.width='100%', out.height='20%', fig.align='center'}
summary <- create_graph() %>%
  add_path(
    n = 6,
    type = "step",
    label = c(
      "Load\\nData",
      "Group\\nSpecies",
      "Select\\nColumn",
      "Estimate\\nMean",
      "Estimate\\nVariance",
      "Make Table"
    ),
    node_aes = node_aes(
      shape = c("square", "circle", "circle", "circle", "circle", "rectangle"),
      width = c(0.75, 0.75, 0.75, 0.75, 0.75, 1.0),
      color = "#3C3C3C",
      fontcolor = "black",
      fillcolor = c("#61acf0", "#f0a561", "#f0a561", "#f0a561", "#f0a561", "#cbd20a"),
      fontname = "Lato"
    ),
    edge_aes = edge_aes(# set edge aesthetics
      color = "#3C3C3C")
  ) %>%
  add_global_graph_attrs(attr = "layout",
                         value = "dot",
                         attr_type = "graph") %>%
  add_global_graph_attrs(attr = "rankdir",
                         value = "LR",
                         attr_type = "graph")

# View the graph in the Viewer
summary %>% render_graph()
```



---

# The Data

The data we will be working with consist of data from the [Rice Rivers Center](https://ricerivers.vcu.edu) which contains water and atmospheric measurements from a stream of sensors in both the James River and on the bluff overlooking the river.

```{r}
library( readr )
url <- "https://docs.google.com/spreadsheets/d/1Mk1YGH9LqjF7drJE-td1G_JkdADOU0eMlrP01WFBT8s/pub?gid=0&single=true&output=csv"
rice <- read_csv( url )
names( rice )
```



---

# The Data

```{r}
head(rice )
```



---

# R Data Workflow

> Describe the daytime air temperatures at the Rice Rivers Center for each weekday of the first week of February, 2014.

--

&nbsp;

To do this, we need to perform the following sequence of general *verb* actions on the data.

```{r echo=FALSE, out.width='90%', out.height='40%', fig.align='center'}
graph <-
  create_graph() %>%
  add_path(
    n = 6,
    type = "step",
    label = c(
      "Load\\nData",
      "Make\\nDates",
      "Find\\nWeekdays",
      "Select\\nColumns",
      "Filter\\nRows",
      "Summarize\\nValues"
    ),
    node_aes = node_aes(
      shape = c("square", "circle", "circle", "circle", "circle", "rectangle"),
      width = c(0.75, 0.75, 0.75, 0.75, 0.75, 0.75),
      color = "#3C3C3C",
      fontcolor = c("black","black","black", "white","black","black"),
      fillcolor = c("#61acf0", "#feae00", "#feae00", "#027104", "#ed220d", "#cbd20a"),
      fontname = "Lato"
    ),
    edge_aes = edge_aes(# set edge aesthetics
      color = "#3C3C3C")
  ) %>%
  add_global_graph_attrs(attr = "layout",
                         value = "dot",
                         attr_type = "graph") %>%
  add_global_graph_attrs(attr = "rankdir",
                         value = "LR",
                         attr_type = "graph")

# View the graph in the Viewer
graph %>% render_graph()
#widgetframe::frameableWidget( graph )
```



---

# Make Date Data Type 🗓

.greeninline[Mutate] the data by adding a new column that is a `Date` object<sup>1</sup>.

```{r}
library( lubridate )
format <- "%m/%d/%Y %I:%M:%S %p"
rice$Date <- parse_date_time( rice$DateTime, 
                              orders=format,
                              tz="EST")
class( rice$Date )
```

--

```{r}
summary( rice$Date )
```




.footnote[<sup>1</sup>See `?strftime` for formatting codes]


---

# Make Date Data Type 🗓

Should make it a Factor so we know ordering.

```{r}
days <- c("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")
rice$Weekday <- weekdays( rice$Date )
rice$Weekday <- factor( rice$Weekday,
                        ordered=TRUE,
                        levels=days)
summary( rice$Weekday )
class( rice$Weekday )
```



---

# 🌡 Fahrenheit to Celsius

.pull-left[

.greeninline[Mutate] the data in-line to create new column.

```{r plot-last, fig.show = 'hide'}
library( ggplot2 )
rice$AirTemp <- (rice$AirTempF - 32 ) * 5 / 9

# Examine the data.

ggplot( rice, aes(x=AirTemp ) )  + 
  geom_histogram( binwidth=1.0, colour = "#333333" ) +
  xlab("Air Temperature (°C)") + ylab("Frequency")
```
]


.pull-right[
```{r ref.label = 'plot-last', echo = FALSE}
```
]


---

# Grab Columns

.orangeinline[Select] the columns of data we will be working with.  

.redinline[And] let's not overwrite our old stuff in case we need to come back.

```{r}
df <- rice[ , c("Date","Weekday","AirTemp", "PAR") ]

# Look at the result

head( df )
```


---

# Filtering Rows 

Two temporal filters are in play here:  

- First week in February  
- Day time


--

```{r}
rice$DateTime[ 25 ]
```

--

```{r}
start_DateTime <- "2/1/2014 12:00:00 AM"
end_DateTime <- "2/7/2014 11:45:00 PM"
start <- parse_date_time( start_DateTime, 
                          orders=format,
                          tz="EST")
end <- parse_date_time( end_DateTime, 
                        orders=format,
                        tz="EST")
c( start, end )
```


---

# Filtering on "First Week of February"

```{r}
df1 <- df[ df$Date >= start & df$Date <= end, ]

# Check the Date Range

summary( df1 )
```

---

# Sunrise🌅 and Sunset 🌆 to get 'daytime'?

&nbsp;

It is amazing how someone records these data and make them available for all of us by a simple search of the internet.

&nbsp;

.pull-left[
![Sunrise 2/1/2014](https://live.staticflickr.com/65535/50381378793_b6517b10fe_w_d.jpg)
![Sunset 2/1/2014](https://live.staticflickr.com/65535/50382255642_a9399a736a_w_d.jpg)
]

.pull-right[
![Sunrise 2/7/2014](https://live.staticflickr.com/65535/50382077786_e59560305e_w_d.jpg)
![Sunset 2/7/2014](https://live.staticflickr.com/65535/50382077716_872bf519a5_w_d.jpg)
]



---

# Hours & Minutes

```{r}
test <- df1[ df1$Weekday == "Monday",]
test$hour <- hour( test$Date ) 
test$minute <- minute( test$Date )
test
```


OK!

---

# Add Hours & Minutes to Filter


```{r}
df3 <- df1
df3$Hour <- hour( df3$Date )
df3$Minute <- minute( df3$Date )
head( df3 )
```



---

# Filter out Pre-Dawn

```{r}
df4 <- df3[ df3$Hour >= 7 & df3$Minute >= 15,]

# Check 
summary( df4 )
```



---

# Filter Out Post-Sundown

Notice that the `hour()` function returns values from 0-23 so `5:30 PM` is denoted as `17:30`.

```{r}
df5 <- df4[ df4$Hour <= 17 & df4$Minute <=30,  ]

# Check
summary( df5 )
```


---
# Just to Make Sure

```{r}
df5[18:24,]
```

&nbsp; 

.center[
Perfectly between sunrise and sunset!  

![Sunrise 2/1/2014](https://live.staticflickr.com/65535/50381378793_b6517b10fe_w_d.jpg)
![Sunset 2/1/2014](https://live.staticflickr.com/65535/50382255642_a9399a736a_w_d.jpg)
]

---

# Select To Remove Extraneous 


```{r}
df6 <- df5[ , c("Date","Weekday", "AirTemp")]
head( df6 )
```


---

# Summarize In Tabular Form

From these raw data, we can create another `data.frame` that has each day of the week as a row and the values for temperature, say as `Minimum`, `Mean`, and `Maximum`.


```{r}
minTemp <- by( df6$AirTemp, day( df6$Date  ), min )
meanTemp <- by( df6$AirTemp, day( df6$Date  ), mean )
maxTemp <- by( df6$AirTemp, day( df6$Date  ), max )
df.table <- data.frame( Minimum = as.numeric( minTemp ), 
                        Average = as.numeric( meanTemp), 
                        Maximum = as.numeric( maxTemp ) )
df.table
```




---

# Set Dates for Each Row

This is kind of a shortcut here.

```{r}
raw_dates <- mdy( paste( "2", 1:7, "2014", sep="/") )
df.table$Weekday <- weekdays( raw_dates  )
df.table
```

---

# Select to Reorder Columns

```{r}
df.table1 <- df.table[ , c(4,1,2,3)]
df.table1
```


---

# Tabular Output

```{r}
library( knitr )
library( kableExtra )
t <- kable( df.table1, digits = 3,
            caption="Table 1: Temperature Ranges for daytime air temperature for the first week of February, 2014 at the Rice Rivers Center in Charles City County, Virginia.")
kable_styling( t )
```



---

# Summarize Graphically

```{r fig.height=3.5, fig.width=10, out.width="100%"}
ggplot( df6, aes(x=Date, y=AirTemp, color=Weekday) ) + 
  geom_line() + 
  geom_point( size  = 3 ) + 
  theme( legend.position = "none" ) 
```







---

# Challenges to Normal R Workflows

The data work flow using indices has several drawbacks including:

- Lots of individual steps, each step divided into many chunks (21 chunks to get the data from Google Drive to the Tablular Output).  
- Uses lots of data frames to hold intermediate options.  We created `r sum( grepl("df",ls())) + 1` data frames in the process of going from `rice` to `df.table`.


--

If you are working with moderately large data sets, this is not a good strategy.


---
class: inverse
background-image: url("https://live.staticflickr.com/65535/50351963133_cffc707725_c_d.jpg")
background-size: contain
background-position: right

.left-column[
.redinline[ 
GGPlot is to built-in  
graphics as $\_\_\_\_\_\_$   
is to build in R  
data work-flows.
 
A) Tidyverse  
B) Tidyverse  
C) Tidyverse, or   
D) Tidyverse
] 
]


---

# Tidyverse

.pull-left[ ![tidy](https://live.staticflickr.com/65535/50295284047_ebb5dec2e8_w_d.jpg) ]


.pull-right[A constellation of Libraries:

- `dplyr`   

- `ggplot2`  

- `purrr`  

- `tibble` 

- several more.
]

All of these libraries have been defined to help you be more effective at data analysis.

---

# Load in the Constilation of Libraries

To get the libraries, first load them in<sup>1</sup>.
```{r message=TRUE}
library( tidyverse)
```

<div class="my-footer"><span><sup>1</sup>If you get an error here saying something like <font class="orangeinline">there is no package called ‘tidyverse’</font> then do <tt>install.packages("tidyverse")</tt> and that shoudl fix it</span></div>


---
# Common Workflow

.middle[

The following general pattern is .fancy[so] common, someone developed a whole package (called `magittr` and it is part of the `tidyverse`) just to make sure we never have to do it the hard way.

&nbsp;

.large[ .fancy[👉 The output of one function becomes the input of another one] ]

]


---
background-image: url("https://live.staticflickr.com/65535/50382456508_bbb16c248d_c_d.jpg")
background-size: fit

???

The Treachery of Images (French: La Trahison des images) is a 1929 painting by Belgian surrealist painter René Magritte. It is also known as This Is Not a Pipe[2] and The Wind and the Song.[3] Magritte painted it when he was 30 years old. It is on display at the Los Angeles County Museum of Art.[1]

The painting shows an image of a pipe. Below it, Magritte painted, "Ceci n'est pas une pipe", French for "This is not a pipe".

> The famous pipe. How people reproached me for it! And yet, could you stuff my pipe? No, it's just a representation, is it not? So if I had written on my picture "This is a pipe", I'd have been lying!  — René Magritte[4]

---

# Pipes In Action

Pipes remove the need a ton of code writing.

.pull-left[
Instead of doing something like this:

```{r eval=FALSE}
df2 <- SOME_OPERATION( df1 )
df3 <- SOME_OTHER_OPERATION( df2 )
df4 <- A_THIRD_OPERATION( df3 )
ggplot( df4, aes(x=...,y=...) ) + geom_point()
```

]

--


.pull-right[
We can instead replace it with the pipe operator (`%>%`) and clean it up considerably.

```{r eval=FALSE}
df1 %>% 
  SOME_OPERATION() %>%
  SOME_OTHER_OPERATION() %>%
  A_THIRD_OPERATION %>%
  ggplot( aes(x=...,y=...) ) + geom_point()
```

Notice:
- .redinline[No] reassigning a bunch of intermediate `data.frame` objects, and
- .redinline[No] need to pass a data.frame to the next function, it is by default the first thing passed in.
]




---

# Example - Tabular Summary


```{r}
df.table1 %>%
  kable( format="html", digits = 2) %>%
  kable_paper( full_width = FALSE ) %>%
  column_spec( 2, color=ifelse( df.table1$Minimum < 0, "blue", ""))
```



---

# Example - Graphical Output


.pull-left[
We can pipe right into a `ggplot()` chain (n.b., the plot elements are still added (+) together and not piped).

&nbsp;

```{r weekly-plot, fig.show='hide'}
df.table1 %>%
  ggplot( aes(x=Weekday,y=Average) ) + 
  geom_col() + 
  ylab("Average Air Temperature (°C)")
```
]

.pull-right[
```{r ref.label='weekly-plot', echo=FALSE}
```
]




---

# The `dplyr` Library

.pull-left[
.center[
![DPlyr](https://live.staticflickr.com/65535/50382551848_ee84ba4b78_o_d.png)  
.fancy[The Grammar of Data Manipulation]
]
]

.pull-right[
The *verbs* are actually `functions` from in `dplyr`:

- Select is done using function `select()`  

- Filter is done using function `filter()`  

- Mutate is done using function `mutate()`  

- Arrange is done using function `arrange()`   

- Group is done using function `group_by()`  

- Summarize is done using function `summarize()`  
]

When combined with `%>%` ...  data magic!

---

# `r fa(name='r-project', fill='steelblue')` Real Example  
### Rice Center Monitoring Data 

So let's grab the Rice Center Data and setp through the process of answering that question:

> Describe the daytime air temperatures at the Rice Rivers Center for the first week of February, 2014.

```{r}
rice <- read_csv( url )
names( rice )
```



---

# Select

Select allows us to grab the column by the name in the `data.frame`.

```{r}
rice %>%
  select( DateTime, AirTempF ) %>%
  head()
```


---

# Selecting to Drop 

To drop columns, you can use the name of the column with a negative sign prepended on it.

```{r}
rice %>%
  select( -RecordID, -SpCond_mScm, -PH_mv, -Depth_ft, -SurfaceWaterElev_m_levelNad83m ) %>%
  names() 
```

---

# Selecting to Rearrange

You can also use it to re-arrange the column order (and because we are lazy, we have the `everything()` function to say 'well, everything else that I haven't already identified).

```{r}
rice %>%
  select( AirTempF, WindDir, Rain_in, everything() ) %>%
  names() 
```


---

# Filter

Filter allows us to select the rows in the data by attributes of the data *within* the table itself.

```{r}
rice %>%
  filter( AirTempF < 32 ) %>%
  head()
```

---

# Mutate

Mutate allows us to change the columns of the data:

```{r}
rice %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders=format,
                                  tz="EST") ) %>%
  mutate( Weekday = factor( weekdays( Date ),
                            ordered=TRUE,
                            levels=days) ) %>%
  mutate( AirTemp = (AirTempF - 32) * 5/9 ) %>%
  select( Date, Weekday, AirTemp) %>%
  summary()
```



---

# Naming Columns Nicely

.pull-left[
It is also possible to use use this to make more readable column names ("Look ma! No `ylab` needed!").  You just have to use the back tick characters to surround the new data column name.

```{r eval=FALSE}
rice %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders=format,
                                  tz="EST") ) %>%
  mutate( `Air Temperature (°C)` = (AirTempF - 32) * 5/9 ) %>%
  select( Date, `Air Temperature (°C)`) %>%
  ggplot( aes( x = Date, y = `Air Temperature (°C)`) ) + 
    geom_line() 
```
]

.pull-right[
```{r echo=FALSE}
rice %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders=format,
                                  tz="EST") ) %>%
  mutate( `Air Temperature (°C)` = (AirTempF - 32) * 5/9 ) %>%
  select( Date, `Air Temperature (°C)`) %>%
  ggplot( aes( x = Date, y = `Air Temperature (°C)`) ) + 
  geom_line() 
```
]




---

# Arrange

Arrange is used to sort the data.


```{r}
rice %>%
  arrange( AirTempF ) %>%
  select( DateTime, AirTempF ) %>%
  head()
```

---

# Reverse Arranging (Deranged perhaps?)

Reversing it (e.g., in descending order) is done by prepending a negative sign.

```{r}
rice %>%
  arrange( -AirTempF ) %>%
  select( DateTime, AirTempF ) %>%
  head()
```



---

# Grouping By A Feature

So here is where we start getting to have some fun.  The `group_by` function partitions the data and is used to create content for the subsequent steps.  Think about the various ways we have used `by()` thus far.  For these, we had to:

1. Identify a column to use as a grouping.  
2. Apply some function to those individual groups.  

--

```{r}
class( rice )
```


---

# Grouping By A Feature

After we make a grouping column and then `group-by()` that column, it gains an additional class type (`grouped_df`).

```{r}
rice %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders=format,
                                  tz="EST") ) %>%
  mutate( Weekday = factor( weekdays( Date ),
                            ordered=TRUE,
                            levels=days) ) %>%
  group_by( Weekday ) %>%
  class() 
```

The overall 'look' of `rice` does not change but it can do cool stuff with `summarize()`.


---

# Summarize 

Summarize allows you to take a bit of the original data and then perform operations on it to create a new `data.frame`.

```{r warning=FALSE}
rice %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders=format,
                                  tz="EST") ) %>%
  mutate( Weekday = factor( weekdays( Date ),
                            ordered=TRUE,
                            levels=days) ) %>%
  group_by( Weekday ) %>%
  summarize( Rain = sum( Rain_in ) )
```

The only columns in the `group_by` and `summarize` statements will be kept and provided as output.



---

.pull-left[
```{r eval=FALSE}
rice %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders="%m/%d/%Y %I:%M:%S %p",
                                  tz="EST") ) %>%
  mutate( Weekday = factor( weekdays( Date ),
                            ordered=TRUE,
                            levels = c("Monday",
                                       "Tuesday",
                                       "Wednesday",
                                       "Thursday",
                                       "Friday",
                                       "Saturday",
                                       "Sunday") ) ) %>%
  mutate( `Temperature (°C)` = (AirTempF - 32) * 5/9 ) %>%
  select( Date, Weekday, `Temperature (°C)`) %>%
  filter( hour( Date ) >= 7 & minute( Date ) >= 15, 
          hour( Date ) <= 17 & minute( Date ) <= 30 ) %>%
  filter( Date >= mdy("2/1/2014") & Date < mdy("2/8/2014") ) %>%
  group_by( Weekday ) %>%
  summarize( Minimum = min( `Temperature (°C)` ),
             Average = mean( `Temperature (°C)`), 
             Maximum = max( `Temperature (°C)` ) ) %>%
  kable( format="html", digits = 2 ) %>%
  kable_paper( full_width = FALSE ) %>%
  column_spec( 2, 
               color=ifelse( df.table1$Minimum < 0, 
                             "blue", ""))
```
]

--

.pull-right[

The output table is: 
```{r echo=FALSE}
rice %>%
  mutate( Date = parse_date_time( DateTime,
                                  orders=format,
                                  tz="EST") ) %>%
  mutate( Weekday = factor( weekdays( Date ),
                            ordered=TRUE,
                            levels=days) ) %>%
  mutate( `Temperature (°C)` = (AirTempF - 32) * 5/9 ) %>%
  select( Date, Weekday, `Temperature (°C)`) %>%
  filter( hour( Date ) >= 7 & minute( Date ) >= 15, 
          hour( Date ) <= 17 & minute( Date ) <= 30 ) %>%
  filter( Date >= mdy("2/1/2014") & Date < mdy("2/8/2014") ) %>%
  group_by( Weekday ) %>%
  summarize( Minimum = min( `Temperature (°C)` ),
             Average = mean( `Temperature (°C)`), 
             Maximum = max( `Temperature (°C)` ) ) %>%
  kable( format="html", digits = 2) %>%
  kable_paper( full_width = FALSE ) %>%
  column_spec( 2, color=ifelse( df.table1$Minimum < 0, "blue", ""))
```
]



















---

class: middle
background-image: url("https://live.staticflickr.com/65535/50367566131_85c1285e2f_o_d.png")
background-position: right
background-size: auto


.pull-left[ ![Moira](https://media.giphy.com/media/7OVR6nwR6CTh2onJq2/giphy.gif) ]
.pull-right[ # 🙋🏻  Questions?

If you have any questions for about<br/> the content presented herein<br/> now is your time.  

If you think of something later though, <br/>feel free to [ask me via email](mailto://rjdyer@vcu.edu) and I'll<br/> get back to you as soon as possible.
]

