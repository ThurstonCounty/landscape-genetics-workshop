---
title: "Data Join Narrative"
output: html_notebook
---

<center>
[![JJ Ying](https://unsplash.com/photos/PDxYfXVlK2M/download?force=true&w=640)](https://unsplash.com/photos/PDxYfXVlK2M?utm_source=unsplash&utm_medium=referral&utm_content=creditShareLink)
</center>

## Overview

Data joins are common features of data manipulation processes including database activities. Joins are important tools for both quantitative data as well as spatial data components, as we'll see later in this workshop.  One of the strengths of `R` is that as long as we can keep our data in well-defined structures such as `data.frames`, it is irrelevant if the data represent measurements, spatial components, polygons, genotypes, or whatever.  

Rarely do we work on only one `data.frame`, particularly when we start working with complex data and data contained within relational databases.  In these cases, data are factored into several tables (akin to `data.frame` objects) with entries that connect the information from one table to another.   

For example.  Consider the situation where we've gone out and samples from different populations and at each population, we collected information for a set of individuals.  When we get back to the lab, we extract DNA, produce genotypes, and look to our analyses which may include importation of additional information for both the population (average rain, temperature, habitat classification, etc) as well as the individual (say 50,000 SNP genotypes, individual coordinates, etc.).

If we are to be working with spatial/ecological aspects of the data set, we probably do not need to have a single EXCEL spreadsheet with all the data together.  It is a waste of your precious computer memory to have potentially gigabites of genotypes in a `data.frame` when you are working on the spatial/ecological aspects of the data set.  It is also irrelevant to the genetic analyses to have all that spatial/ecological/climatological data in the same `data.frame` as the genotypes when looking at genetic aspects.  As such, it is more efficient if we keep the data in their formats but provide a mechanims that links them togther.  This is what joins do.  


## Leaning Objective

At the end of this activity, you should be funcationally literate in the following skills:  

- Identifying foreign and primary keys in linked data.
- Describe and perform `left`, `right`, `inner`, `outer`, `semi`, and `anti` joins on complex data.


## Required Components

This activity will use the following libraries.  If you do not have them already installed, please do so now using `install.packages()`.

```{r}
library( knitr )
library( tidyverse )
library( nycflights13 )
```



## Tables

For the purpose of this activity, I will use the term `table` to be synonymous with a `data.frame` in `R`.   Consider the two tables represented in the figure below, each of which have a key and a value.

<center>
![Example data table structure](https://live.staticflickr.com/65535/50427672632_24e45139a8_c_d.jpg)
</center>

Each has a column I named *Key* and another with some data in it.  In `R` they could be defined as:

```{r echo=TRUE}
df.X <- data.frame( Key = c("A","B","C"),
                    X = c(1,2,3) )
df.X
```
and 

```{r echo=TRUE}
df.Y <- data.frame( Key = c("B","C","D"),
                    Y = c(10,11,12) )
df.Y
```


## Keys

An important component of relational data are the *keys*.  These are unique identifiers for a particular datum from a table.  In each of these examples the varible (obviously named) `Key` is what is called a *Primary Key* because it uniquely identifies each row.  You can verify this by counting the number of entries then filtering only for ones with 2 or more instances.

```{r}
df.X %>%
  count( Key ) %>%
  filter( n > 1 )
```

Notice there is nothing here as each is unique.

> The column `Key` is a Primary Key for the `df.X` data because it identifies a unique row *in that table*.

In addition to a *Primary Key* we can have a *Foreign Key* when it is used to indicate data within a separate table.  For example, if I am interested to see if the smallest value in `df.X$X` corresponds with the smallest value in `df.Y$Y`, then I will be using the `Key` form `df.X` representing `max(X)` to find the value of `Y` in `df.Y` and evaluate if it is `max(Y)`.  This means that `df.X$Key` is a *Foreign Key* as it points to a row in the `df.Y` data frame.

> A `Key` is a foreign key if it references the unique `Primary Key` *in a different table*.

The keys are used to link together different tables.




# Joins

> A *join* is where we combine information contained within two data frames.  

Joins are ways to merge together data and come in four flavors.  

## Left Join

A *left join* is one where all the data from the left data frame is in the result and the data whose keys in the right data frame are present in the left one are also included.  Graphically, this leads to:


![left join](https://live.staticflickr.com/65535/50427817371_678f0f64c7_c_d.jpg)

Where in `R` we do this using the `left_join()` function. 

```{r}
df.X %>%
  left_join( df.Y, by="Key")
```



## Right Join

The right join does the same thing but keeps all the keys in the right data table and has missing data where the key in the left one is not in the right one.

![Right Join](https://live.staticflickr.com/65535/50427125528_0de6281475_c_d.jpg)

This is accomplished using the `right_join()` function.

```{r echo=TRUE}
df.X %>%
  right_join( df.Y, by="Key")
```



## Full (or Outer) Join

This join is one where all the keys are retained adding missing data as necessary.


![Outer Join](https://live.staticflickr.com/65535/50427993992_4ccede1574_c_d.jpg)

```{r}
df.X %>%
  full_join( df.Y, by="Key")
```


## Inner Join

The last one retains *only* those keys that are common in both.

![Inner Join](https://live.staticflickr.com/65535/50427125683_ac44eb1500_c_d.jpg)

```{r}
df.X %>%
  inner_join( df.Y, by="Key")
```


# Filtering Joins

We can also use joins to filter values within one `data.frame`.  Here the `semi_join()` keeps everything in the left data that has a key in the right one, but **importantly** it does not import the right data columns into the result.


```{r}
df.X %>%
  semi_join( df.Y )
```

The opposite of this is the `anti_join()` which drops everything in the left table that has a key in the right one, leaving only the ones that are unique.

```{r}
df.X %>%
  anti_join( df.Y )
```



## Step-By-Step Examples

The following examples use the data contained in the `nycflights13` package.


### Positional Joins

*What are the five airlines with the largest arrival delays?*  

For this, we need two of the `data.frames` in the package, the `flights` one and the `carriers` one.   

1. From `flights` group the data by carrier.
2. Find the average delay (here I'll use the arrival delay).  
3. Sort the data in decreasing order.  
4. Left join with the `airlines` using the key `carrier` (as primary in `airlines` but foreign in `flights`)
5. Select the two columns to print out
6. Pass to an appropriate table.

```{r}
flights %>%
  group_by( carrier ) %>%
  summarize( Delay = mean( arr_delay, na.rm=TRUE) ) %>%
  arrange( -Delay ) %>%
  left_join( airlines, by="carrier") %>%
  select( Carrier = name, Delay) %>%
  head( n = 5 ) %>%
  knitr::kable( digits=3, caption = "Average delay by carrier for flights leaving NYC airports in 2013.") %>%
  kableExtra::kable_styling( full_width = FALSE )
```

